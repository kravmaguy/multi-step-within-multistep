import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { MainDetailsActionType } from "../redux/reducers/driverReducer";
import { stepActionType } from "../redux/reducers/StepReducer";
import { Header } from "./Header/Header.jsx";
// import Button from "./Button";
import { useHistory, useLocation } from "react-router-dom";

function StepDriver() {
  const history = useHistory();

  const step = useSelector((state) => state.StepReducer.step);
  const MainDetails = useSelector((state) => state.MainDetails);
  const { firstName, lastName, phone, email, liscense } = MainDetails;
  const [fName, setfName] = useState(firstName);
  const [lName, setlName] = useState(lastName);
  const [PHONE, setPhone] = useState(phone);
  const [EMAIL, setEmail] = useState(email);
  const [LISCENSE, setLiscense] = useState(liscense);
  const dispatch = useDispatch();
  const location = useLocation();
  console.log("loca", location);

  useEffect(() => {
    if (location.pathname === "/") {
      history.push("driver/");
    }
    if (location.pathname.includes("2")) {
      dispatch({
        type: stepActionType.setStep,
        payload: 2,
      });
    }
  }, [dispatch, location.pathname]);

  const submitValue = (e) => {
    e.preventDefault();
    const frmdetails = {
      firstName: fName,
      lastName: lName,
      phone: PHONE,
      email: EMAIL,
    };

    dispatch({
      type: MainDetailsActionType.details_save,
      payload: frmdetails,
    });

    if (step === 1) {
      dispatch({
        type: stepActionType.setStep,
        payload: step + 1,
      });
      return history.push("/driver/2");
    }

    if (step === 2) {
      return history.push("/Table");
    }
  };

  const firstForm = () => {
    dispatch({
      type: stepActionType.setStep,
      payload: 1,
    });
  };
  return (
    <>
      <Header />
      <hr />
      {step === 1 ? (
        <>
          <input
            type="text"
            placeholder="First Name"
            onChange={(e) => setfName(e.target.value)}
            value={fName}
          />
          <input
            type="text"
            placeholder="Last Name"
            onChange={(e) => setlName(e.target.value)}
            value={lName}
          />
          <input
            type="text"
            placeholder="Phone"
            onChange={(e) => setPhone(e.target.value)}
            value={PHONE}
          />
          <input
            type="text"
            placeholder="Email"
            onChange={(e) => setEmail(e.target.value)}
            value={EMAIL}
          />
        </>
      ) : (
        <>
          <input
            type="text"
            placeholder="Liscense"
            onChange={(e) => setLiscense(e.target.value)}
            value={LISCENSE}
          />
          <button onClick={firstForm}>Prev</button>
        </>
      )}
      <button onClick={submitValue}>"NEXT"</button>
    </>
  );
}

export default StepDriver;
