import { Form } from "./Form/Form";
import { BrowserRouter as Router } from "react-router-dom";
import { Header } from "./Header/Header";
import { Stepper } from "./Stepper/Stepper";
import styles from "./View.module.css";

export function View() {
  return (
    <main className={styles.main}>
      <div className={styles.container}>
        <Router>
          <Header />
          <div className={styles.wrapper}>
            <Stepper />
            <Form />
          </div>
        </Router>
      </div>
    </main>
  );
}
