import styles from "./Form.module.css";
import { Route, Switch } from "react-router-dom";
import StepsComplete from "../StepsComplete";
import StepDriver from "../StepDriver";
import StepMultiSelector from "../StepMultiSelector";
import StepTable from "../StepTable";
import { useSelector } from "react-redux";
import { stepsState } from "./../../redux/reducers/StepReducer";

export function Form() {
  const step = useSelector((state) => state.StepReducer.step);
  const calcStep = (newStep) => {
    return newStep > 1 ? (newStep -= 1) : step;
  };

  return (
    <div className={styles.container}>
      <h1 className={styles.h1}>
        {`step ${calcStep(step)} ${step === 2 ? "b" : ""}/${
          stepsState.length - 1
        }`}
        <strong className={styles.strong}>{stepsState[step - 1].name} </strong>
      </h1>
      <form>
        <p style={{ color: "white" }}> Multi step form here</p>
        <Switch>
          <Route path="/driver">
            <StepDriver />
          </Route>
          <Route path="/vehicle">
            <StepMultiSelector />
          </Route>
          <Route exact path="/table">
            <StepTable />
          </Route>
          <Route path="/complete">
            <StepsComplete />
          </Route>
          <Route path="/">
            <StepDriver />
          </Route>
        </Switch>
      </form>
    </div>
  );
}
