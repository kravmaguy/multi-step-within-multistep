import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { stepActionType } from "../redux/reducers/StepReducer";

const StepTable = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  // const step = useSelector((state) => state.StepReducer.step);

  useEffect(() => {
    dispatch({
      type: stepActionType.setStep,
      payload: 3,
    });
  }, [dispatch]);

  const submitValue = (e) => {
    e.preventDefault();
    history.push("/Vehicle");
  };

  const [data, setData] = useState([]);
  const [startStep, setStep] = useState(1);
  const [loadingData, setLoadingData] = useState(true);

  useEffect(() => {
    async function getData(start = 1) {
      await axios
        .get(
          `https://jsonplaceholder.typicode.com/todos?_page=${startStep}&_limit=3`
        )
        .then(({ data }) => {
          setData(data);
          setLoadingData(false);
        });
    }
    if (loadingData) {
      getData();
    }
  }, [startStep, loadingData]);

  const heads = data.length ? Object.keys(data[0]) : null;

  const genTableHeads = () => {
    return (
      <tr>
        {heads.map((head) => (
          <td key={head}>{head}</td>
        ))}
      </tr>
    );
  };
  const genTableRows = () => {
    return (
      <>
        {data.map((data, idx) => (
          <tr key={idx}>
            <td>{data[heads[0]]}</td>
            <td>{data[heads[1]]}</td>
            <td>{data[heads[2]]}</td>
            <td>{!data[heads[3]] ? "false" : "true"}</td>
          </tr>
        ))}
      </>
    );
  };

  const handleNext = (e) => {
    e.preventDefault();
    setLoadingData(true);
    setStep((startStep) => startStep + 1);
  };

  const handlePrev = (e) => {
    e.preventDefault();
    setLoadingData(true);
    setStep((startStep) => startStep + -1);
  };

  return (
    <div>
      <>
        {startStep > 1 && (
          <button disabled={loadingData} onClick={(e) => handlePrev(e)}>
            Prev
          </button>
        )}
        <button disabled={loadingData} onClick={(e) => handleNext(e)}>
          Next
        </button>
        {!loadingData ? (
          <div id="table-controls-container">
            <table>
              <tbody>
                {heads.length ? genTableHeads() : null}
                {data.length && heads.length ? genTableRows() : null}
              </tbody>
            </table>
          </div>
        ) : (
          <p>loading...</p>
        )}
      </>

      <button onClick={() => history.push("/driver/2")}>Prev</button>

      <button onClick={submitValue}>Next</button>
    </div>
  );
};

export default StepTable;
