import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { stepActionType } from "../redux/reducers/StepReducer";
import Select from "react-select";

const fetchCountries = (url) => {
  return axios
    .get(url)
    .then(({ data }) => data)
    .catch((err) => console.error(err));
};

const VehicleSelector = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    dispatch({
      type: stepActionType.setStep,
      payload: 4,
    });
  }, [dispatch]);

  const [cars, setCars] = useState([]);
  const [loaded, setIsLoaded] = useState(false);

  useEffect(() => {
    const url =
      "https://vpic.nhtsa.dot.gov/api/vehicles/getallmakes?format=json";
    fetchCountries(url)
      .then(({ Results }) => {
        const results = Results.slice(0, 500);
        const CarsSelect = results.map((element) => {
          return { label: element.Make_Name, value: element.Make_Name };
        });
        console.log(Array.isArray(CarsSelect), "cars Select");
        setCars(CarsSelect);
        setIsLoaded(true);
      })
      .catch((e) => {
        console.log(e, "e error");
      });
  }, []);

  return (
    <>
      <button onClick={() => history.push("/Table")}>Prev</button>

      {loaded ? (
        <Select
          defaultValue={cars[0]}
          isMulti
          name="colors"
          options={cars}
          className="basic-multi-select"
          classNamePrefix="select"
          onChange={(e) => console.log(e, "selected")}
        />
      ) : (
        <option value="Loading...">Getting Cars data please wait...</option>
      )}
    </>
  );
};

export default VehicleSelector;
