import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { stepActionType } from "../redux/reducers/StepReducer";

const fetchCountries = (url) => {
  return axios
    .get(url)
    .then(({ data }) => data)
    .catch((err) => console.error(err));
};

const VehicleSelector = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    dispatch({
      type: stepActionType.setStep,
      payload: 4,
    });
  }, [dispatch]);

  const [countries, setCountries] = useState([]);
  const [loaded, setIsLoaded] = useState(false);

  useEffect(() => {
    const url =
      "https://vpic.nhtsa.dot.gov/api/vehicles/getallmakes?format=json";
    fetchCountries(url)
      .then(({ Results }) => {
        const results = Results.slice(0, 500);
        const CarsSelect = results.map((element) => {
          return { alpha2Code: element.Make_ID, name: element.Make_Name };
        });
        setIsLoaded(true);
        setCountries(CarsSelect);
      })
      .catch((e) => {
        console.log(e, "e error");
      });
  }, []);

  return (
    <>
      <button onClick={() => history.push("/Table")}>Prev</button>
      <input type="text" list="cars" />
      <datalist id="cars" className="vehicles-select" type="text">
        {loaded ? (
          countries.map((option) => (
            <option key={option.alpha2Code} value={option.name}>
              {option.name}
            </option>
          ))
        ) : (
          <option value="Loading...">Getting Cars data please wait...</option>
        )}
      </datalist>
    </>
  );
};

export default VehicleSelector;
