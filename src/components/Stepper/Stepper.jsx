import { Step } from "./Step";
import styles from "./Stepper.module.css";
import { stepsState } from "./../../redux/reducers/StepReducer";

export function Stepper() {
  return (
    <div className={styles.container}>
      {stepsState.map(
        (step) => !step.isMulti && <Step key={step.step} id={step.step} />
      )}
    </div>
  );
}
