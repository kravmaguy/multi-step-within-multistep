import styles from "./Step.module.css";
import { FaMapMarkerAlt } from "react-icons/fa";
import { useSelector } from "react-redux";
import { stepsState } from "./../../redux/reducers/StepReducer";

export function Step({ id }) {
  const step = useSelector((state) => state.StepReducer.step);
  console.log(stepsState, "stepSteat");
  return (
    <div className={styles.container}>
      <div className={styles.title}>{stepsState[id - 1].name}</div>
      <div
        className={`${styles.icon} ${
          step === id || (step === 2 && id === 1) ? styles.active : ""
        }`}
      >
        <FaMapMarkerAlt size={20} />
      </div>
      <div
        className={`${styles.dot} ${step === id ? styles.active : ""}`}
      ></div>
    </div>
  );
}
