export const stepsState = [
  { name: "personal details", completed: false, step: 1, next: 2 },
  {
    name: "liscense details",
    completed: false,
    step: 2,
    prev: 1,
    next: 3,
    isMulti: true,
  },
  { name: "table select", completed: false, step: 3, prev: 2, next: 4 },
  {
    name: "vehicles multi-select",
    completed: false,
    step: 4,
    prev: 3,
    next: 5,
  },
];

const completed = Array(stepsState.length).fill(false);

const initialState = {
  completed,
  step: 1,
  stepsState,
};

export const stepActionType = {
  setStep: "set/step",
};

export function StepReducer(state = initialState, action) {
  switch (action.type) {
    case stepActionType.setStep:
      return { ...state, step: action.payload };
    default:
      return state;
  }
}
