import { combineReducers } from "redux";
import { TableReducer } from "./tableReducer";
import { StepReducer } from "./StepReducer";
import { MainDetailsReducer } from "./driverReducer";

export const rootReducer = combineReducers({
  MainDetails: MainDetailsReducer,
  StepReducer: StepReducer,
  Table: TableReducer,
});
