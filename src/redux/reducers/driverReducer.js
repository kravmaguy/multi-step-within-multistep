const initialState = {
  firstName: "",
  lastName: "",
  phone: "",
  email: "",
  liscense: "",
};

export const MainDetailsActionType = {
  details_save: "driver/name/save",
  driver_license_save: "driver/license/save",
};

export function MainDetailsReducer(state = initialState, action) {
  switch (action.type) {
    case MainDetailsActionType.details_save:
      return { ...state, ...action.payload };
    case MainDetailsActionType.driver_license_save:
      return { ...state, ...action.payload };
    case "Reset_Driver":
      return { ...initialState };
    default:
      return state;
  }
}
