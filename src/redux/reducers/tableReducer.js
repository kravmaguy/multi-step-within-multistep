const initialState = {
  headers: [],
  val1: "",
  val2: "",
  val3: "",
};

export const tableActionType = {
  table_save: "table/data/save",
  update: "table/update",
};

export function TableReducer(state = initialState, action) {
  switch (action.type) {
    case tableActionType.table_save:
      return { ...state, val1: action.payload };
    //   case MainDetailsActionType.driver_license_save:
    //     return { ...state, ...action.payload };
    default:
      return state;
  }
}
